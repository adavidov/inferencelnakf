#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  7 17:42:06 2020

@author: andjela
"""

import numpy as np
import matplotlib.pyplot as plt

SMALL_SIZE = 12
MEDIUM_SIZE = 14
BIGGER_SIZE = 16

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


import os
import scipy.io

g2c6 = scipy.io.loadmat('CompTimesFor-1021x6-t0-Group2.mat')
g2c12 = scipy.io.loadmat('CompTimesFor-1021x12-t0-Group2.mat')
g2c18 = scipy.io.loadmat('CompTimesFor-1021x18-t0-Group2.mat')
g2c24 = scipy.io.loadmat('CompTimesFor-1021x24-t0-Group2.mat')
g2c30 = scipy.io.loadmat('CompTimesFor-1021x30-t0-Group2.mat')

g2 = [g2c6,g2c12,g2c18,g2c24,g2c30]

gac6 = scipy.io.loadmat('CompTimesFor-1021x6-t0-allGroups.mat')
gac12 = scipy.io.loadmat('CompTimesFor-1021x12-t0-allGroups.mat')
gac18 = scipy.io.loadmat('CompTimesFor-1021x18-t0-allGroups.mat')
gac24 = scipy.io.loadmat('CompTimesFor-1021x24-t0-allGroups.mat')
gac30 = scipy.io.loadmat('CompTimesFor-1021x30-t0-allGroups.mat')

ga = [gac6,gac12,gac18,gac24,gac30]
nc = [6, 18, 30]
a = 0.3

nt = g2[1]['compTime'][:,0]

fig1= plt.figure()
for i,n in enumerate(nc):
    llLNA = g2[i]['compTime'][:,1]
#    llLNA = np.log2(g2[i]['compTime'][:,1])
    plt.plot(nt, llLNA, 'C0-o', alpha=a*(i+1), label=f'LNA {nc[i]} cells')
for i,n in enumerate(nc):
    llLNAKF = g2[i]['compTime'][:,3]
#    llLNAKF = np.log2(g2[i]['compTime'][:,3])
    plt.plot(nt, llLNAKF, 'C1-o', alpha=a*(i+1), label=f'LNA+KF {nc[i]} cells ')
plt.xlabel('number of measurements')
plt.ylabel('computational times (s)')
plt.yscale('log')
plt.grid(True)
plt.legend()
plt.show()
#plt.savefig(f'{filesavebase}comptimes-G1.pdf')

fig2= plt.figure()
for i,n in enumerate(nc):
    llLNA = ga[i]['compTime'][:,1]
#    llLNA = np.log2(ga[i]['compTime'][:,1])
    plt.plot(nt, llLNA, 'C0-o', alpha=a*(i+1), label=f'LNA {nc[i]} cells')
for i,n in enumerate(nc):
    llLNAKF = ga[i]['compTime'][:,3]
#    llLNAKF = np.log2(ga[i]['compTime'][:,3])
    plt.plot(nt, llLNAKF, 'C1-o', alpha=a*(i+1), label=f'LNA+KF {nc[i]} cells')
plt.xlabel('number of measurements')
plt.ylabel('computational times (s)')
plt.grid(True)
plt.yscale('log')
plt.legend()
plt.show()
#plt.savefig(f'{filesavebase}comptimes-G0.pdf')

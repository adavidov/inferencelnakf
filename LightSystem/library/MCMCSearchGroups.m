function MCMCSearchGroups(filename_llchain, numProc, ...
    ChainLength, givenParas, G, numberOfMeasurements, numtraj,...
    cumnumCells, measurestep, Tmax, uIn, Y_all, S_all)

    
    disp("Starting parameters:")
    paramsOld = givenParas;

    % Likelihood calculation for starting params
    llOld = LikelihoodGroups(paramsOld, G, numberOfMeasurements, numtraj,...
        cumnumCells, measurestep, Tmax, uIn, Y_all, S_all)

    disp("Let's try to find better.")

    
    
    %set matlab number of processors use
    LASTN = maxNumCompThreads(numProc);

    %ChainLength is already given as an input

    %number of parameters to be estimated
    numberOfParamsToEstimate = 8;

    %evolving parameter estimates
    paramsChain = zeros(numberOfParamsToEstimate, ChainLength);

    %evolving likelihoods
    llChain=zeros(1,ChainLength);


    % Initialize the chain
    paramsChain(1:end,1)=paramsOld;
    llChain(1)=llOld;


    % define how much we want to move our parameters from the old position
    std0=ones(numberOfParamsToEstimate,1); 
    stdM = 0.02;  
    sigmaRate=stdM.*std0;   

    paramsNew=zeros(1, numberOfParamsToEstimate);

    for chain=2:ChainLength    
        disp(" ****************************** Chain: "+chain)
        save(filename_llchain, 'paramsChain',  'llChain');


        forwardProb = 1.0;
        backwardProb = 1.0;

        %for kpp=1:numberOfParamsToEstimate 
        for kpp=setdiff(1:numberOfParamsToEstimate,[1,6,7])
            paramsNew(kpp) = lognrnd(log(paramsOld(kpp)), sigmaRate(kpp));
            forwardProb = forwardProb * pdf('logn', paramsNew(kpp), log(paramsOld(kpp)), sigmaRate(kpp));
            backwardProb = backwardProb * pdf('logn', paramsOld(kpp), log(paramsNew(kpp)), sigmaRate(kpp));
        end

       %Uniform distribution on c2 to be between 0 and 15
        kpp=1;
        paramsNew(kpp) = -1;
        while ( paramsNew(kpp) < 0 || paramsNew(kpp) > 15 )
            paramsNew(kpp) = lognrnd(log(paramsOld(kpp)), sigmaRate(kpp));
        end
        forwardProb = forwardProb * pdf('logn', paramsNew(kpp), log(paramsOld(kpp)), sigmaRate(kpp));
        backwardProb = backwardProb * pdf('logn', paramsOld(kpp), log(paramsNew(kpp)), sigmaRate(kpp));
        
       %Uniform distribution on Kd to be between 0 and 1
        kpp=7;
        paramsNew(kpp) = -1;
        while ( paramsNew(kpp) <0 || paramsNew(kpp) >1 )
            paramsNew(kpp) = lognrnd(log(paramsOld(kpp)), sigmaRate(kpp));
        end
        forwardProb = forwardProb * pdf('logn', paramsNew(kpp), log(paramsOld(kpp)), sigmaRate(kpp));
        backwardProb = backwardProb * pdf('logn', paramsOld(kpp), log(paramsNew(kpp)), sigmaRate(kpp));
        
        %Uniform distribution on nH to be between 0 and 10
        kpp=6;
        paramsNew(kpp) = -1;
        while ( paramsNew(kpp) <0 || paramsNew(kpp) >10 )
            paramsNew(kpp) = lognrnd(log(paramsOld(kpp)), sigmaRate(kpp));
        end
        forwardProb = forwardProb * pdf('logn', paramsNew(kpp), log(paramsOld(kpp)), sigmaRate(kpp));
        backwardProb = backwardProb * pdf('logn', paramsOld(kpp), log(paramsNew(kpp)), sigmaRate(kpp));

        %New parameters set
        %paramsNew;  

        try
        %  Likelihood calculation for new parameters
        llNew = LikelihoodGroups(paramsNew, G, numberOfMeasurements, numtraj,...
            cumnumCells, measurestep, Tmax, uIn, Y_all, S_all)       

        % Metropolis-Hastings acceptance criterion
            MHa = min(1, exp(llNew-llOld)* backwardProb / forwardProb);

            disp("llOld = " + llOld);
            disp("llNew = " + llNew);
            disp("MHa = " + MHa);

            if (rand < MHa)
                llChain(chain)=llNew;
                llOld=llNew;
                paramsChain(:,chain)=paramsNew;
                paramsOld=paramsNew;
                         disp('      Accepted')
            else
                paramsChain(:, chain) = paramsOld;
                llChain(chain)=llOld;
                        disp('      Rejected1')
            end

          catch
             paramsChain(:, chain) = paramsOld;
             llChain(chain)=llOld;
                      disp('      Rejected2')
        end

    end

    save(filename_llchain, 'givenParas', 'paramsChain',  'llChain');

end


function [momentsOpenLNAcenteredG,meanModelG,CovMatG,Ht,Lt] = LightSystemLNAOpen(uIn,measurestep,numberOfMeasurements,paras)
    % Y_all = 1xG cells, each cell contains matrix of the size numtrajperGr x numberofmeasurements 
   
    G = size(uIn,1);
    %numberOfMeasurements = size(uIn,2);
    Tmax = numberOfMeasurements*measurestep;
    %declare variables to save
    momentsOpenLNAcenteredG = cell(1,G);
    meanModelG = cell(1,G);
    CovMatG = cell(1,G);
  
    
    % uIn is a matrix of light for each group of a size G x numberofmeasurements
    % for a given set of parameters we can precompute hill function values
    % for each time step
    c2 = paras(1);
    a = paras(2);
    b = paras(3);
    s = paras(4);
    muE = paras(5);
    nH = paras(6);
    Kd = paras(7);
    h2 = paras(8);

    
    for g = 1:G
        %build hill functions based on light  
        H0 = 0;
        [Htg, Ltg] = HillFunctionGroups(measurestep,Tmax,uIn(g,:), c2, nH, Kd, H0);
        Ht{g} = Htg;
        Lt{g} = Ltg; %every minute solution, not every measurestep        
        y0 = [muE; 0; muE^2; 0; 0]; %; 0];  
        tspan = 0:measurestep:(numberOfMeasurements-1)*measurestep;
        [momuncentered] = moments2ndLNAwithHiddenODEall(tspan,paras,y0,Ltg);
        mX = momuncentered(:,1);
        mY = momuncentered(:,2);
        meanModel = s*mY;
        CovMat = zeros(length(tspan),length(tspan));
        for l=1:length(tspan)
            %disp([ g, l, length(tspan)])
            VendXX = momuncentered(l,3) - momuncentered(l,1)^2;
            VendXY = s*(momuncentered(l,4) - momuncentered(l,1)*momuncentered(l,2));
            VendYY = s^2*(momuncentered(l,5) - momuncentered(l,2)^2);
            Vend = [VendXX,VendXY;VendXY,VendYY];        
            CovMat(l,l) =  VendYY;
            for m=l+1:length(tspan)
                tDiff = (l-1)*measurestep:measurestep:(m-1)*measurestep;% !!!! important for light
                [momentsOpenLNAcorr] = moments2ndLNAwithHiddenODECorr(tDiff,paras,mX(l),mY(l),Ltg);
                TimeCorrDyn = reshape(momentsOpenLNAcorr,2,2);
                TimeCorr = Vend*TimeCorrDyn';
                CovMat(l,m) = TimeCorr(2,2);
                CovMat(m,l) = CovMat(l,m);
            end
        end
        
        %Note: if first time point is deterministic it has variance zero and is uncorrelated with everything
        CovMatB = CovMat(2:end,2:end);
        meanModelB = meanModel(2:end);
        meanModelG{g} = meanModelB;
        CovMatG{g} = CovMatB;
        momentsOpenLNAcenteredG{g} = momuncentered;        
    end
end
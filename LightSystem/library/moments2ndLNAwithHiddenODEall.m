function [xEnd] = moments2ndLNAwithHiddenODEall(tspan,paras,y0,Lt)

    %paras = [c2,a,b,s,muE,nH,kappa,h2];

    c2 = paras(1);
    a = paras(2);
    b = paras(3);
    s = paras(4);
    muE = paras(5);
    nH = paras(6);
    Kd = paras(7);
    h2 = paras(8);


    h1 = muE*h2;
        
    options = odeset('RelTol',1e-6);   
    
    nTime = length(tspan);
    Ltt = Lt(tspan+1);
    y_list = zeros(nTime, length(y0));
    y_list(1,:) = y0;
    
    for iTime = 2:nTime   
        L = Ltt(iTime);
        [t_,y_] = ode45(@pre,tspan(iTime-1:iTime),y0,options);
        y0 = y_(end, :);
        y_list(iTime, :) = y0;
    end
    
    
    function y = pre(t,y)
        M = y(1);
        P = y(2);
        MM = y(3);
        MP = y(4);
        PP = y(5);

        y = [ h1 - M*h2,...
            M*a*L - P*b,...
            h1 + 2*M*h1 + M*h2 - 2*MM*h2,...
            P*h1 - MP*h2 - MP*b + MM*a*L,...
            P*b - 2*PP*b + M*a*L + 2*MP*a*L];

        y = y';
        
    end

    xEnd = y_list;
end
    
    

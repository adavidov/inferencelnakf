function [x0,CovMatKF] = KalmanUpdate(xEnd,sigma,P,dataPoint)
        
        meanPred = [xEnd(1) xEnd(2)];
        
        CovMatPred = [xEnd(3)-xEnd(1)^2,xEnd(4)-xEnd(1)*xEnd(2); ...
                        xEnd(4)-xEnd(1)*xEnd(2),xEnd(5)-xEnd(2)^2];

    % KF update
        %take the noise estimate
        R = sigma^2;
        KGain = CovMatPred*P'*(P*CovMatPred*P'+R)^(-1);

        meanKF = meanPred' + KGain*(dataPoint-meanPred*P'); 
        meanKF = max(0,meanKF);
        
        CovMatKF = (eye(2)-KGain*P)*CovMatPred;
        CovMatKF(1,1) = max(0,CovMatKF(1,1));
        CovMatKF(2,2) = max(0,CovMatKF(2,2));

        % return uncentered moments for LNA calculations
        x0 = [meanKF(1);
            meanKF(2);
            CovMatKF(1,1)+meanKF(1)^2;
            CovMatKF(1,2)+meanKF(1)*meanKF(2);
            CovMatKF(2,2)+meanKF(2)^2];
end


% Here we run the likelihood computations using LNA+KF of parameters on the data, assumed to come from
% a process similar to the model from Nature paper
% 0 --h1-->  E --h2--> 0
% 0 --aE(t)L(t)--> F --b--> 0
% L(t) = ( H(t) * c2 )^nH / (kappa + (H(t) * c2)^nH)
% d/dt H(t) = u(t - tau_delay) - c2 H(t)
% E(t) is interpreted as the randomly fluctuating  cell  responsiveness to
% green light
% 
% h2 - hidden time scale
% h1 - muE*h2
% VarE = muE
% 
% In this case: there are several groups of different light signal. 
% For group g: U(g) = [ 1 0 ... 1 0 ] etc
% Light signals and data we have. 

% we want to infer the rest of parameters for this given data set: 
% c2, a, b, nH,Kd, muE, h2, and s

function [llNew] = LikelihoodGroups(paramsNew, G, numberOfMeasurements, numtraj, ...
    cumnumCells, measurestep, Tmax, uIn, Y_all, S_all) 

    c2New = paramsNew(1);
    aNew = paramsNew(2);
    bNew = paramsNew(3);
    sNew = paramsNew(4);
    muENew = paramsNew(5);
    nHNew = paramsNew(6);
    KdNew = paramsNew(7);
    h2New = paramsNew(8);
    disp( "c2New = " + c2New+ ", aNew = " + aNew+ ", bNew = " + bNew + ", sNew = " + sNew+...
        ", muENew = " + muENew+ ", nHNew = " + nHNew+ ", KdNew = " + KdNew + ", h2New = " + h2New)

    
    P = [0, sNew];
    
    %build hill functions based on light  
    Ht = cell(1,G);
    Lt = cell(1,G);
    for g = 1:G
        H0 = 0;
        [Htg, Ltg] = HillFunctionGroups(measurestep,Tmax,uIn(g,:), c2New, nHNew, KdNew, H0);
        Ht{g} = Htg;
        Lt{g} = Ltg;
    end
    
    %likelihood of proposal (up to constant)
    llLNAnAll = 0;

    parfor k = 1:numtraj
        %for this trajectory 
        g=min(find(k<=cumnumCells));
        if g>1; kloc = k - cumnumCells(g-1); else; kloc = k; end
        %disp(k);
        llLNAn = 0;
        
        % initialisation of the moments
        % simulated data
        xEnd = [muENew; 0; muENew^2; 0; 0];  
       
       % experimental data
       % xEnd = [muENew; 0; muENew+muENew^2; 0; 0];  

        % for every measurement step                    

        for m = 1:numberOfMeasurements-1
                disp("m = ", m, "xEnd = ", xEnd)
            %KF update
                % returns uncentered moments for LNA calculations
                sigma = S_all{g}(m);
                dataPoint = Y_all{g}(kloc,m);
                [x0,CovMatKF] = KalmanUpdate(xEnd,sigma,P,dataPoint);
                
                tspan = (m-1)*measurestep:m*measurestep;
                [xEnd] = moments2ndLNAwithHiddenODE(tspan,paramsNew,x0,Lt{g}); 

                
            %update LIKELIHOOD at measurement times
                meanModel = sNew*xEnd(2);
                sigma = S_all{g}(1,m+1); 
                CovMat = sNew*sNew*(xEnd(5)-xEnd(2)^2) + sigma^2; 
                dataAll = Y_all{g}(kloc,m+1);

                llLNATrans = -1/2*(log(det(CovMat)) +...
                    transpose(meanModel-dataAll)*((CovMat)\(meanModel-dataAll)) +...
                    length(meanModel)*log(2*pi)); 
       
                
                %correction for the bad search
                if ((not(isreal(llLNATrans))) || (x0(1) < 0) || (x0(2) < 0 ) || (xEnd(1) < 0 ) || (xEnd(2) < 0))
		             disp("Correction on: cell of group "+g+", trajectory " +kloc+", and measurement "+m)
                     disp("llLNATrans = " + llLNATrans)
                     disp("dataPoint = " + dataPoint)
                     disp("x0 = [" + x0(1) + ", " +x0(2) + ", " + x0(3) + ", " +x0(4) +", " +x0(5)+ "]")
                     disp("xEnd = [" + xEnd(1) + ", " +xEnd(2) + ", " + xEnd(3) + ", " +xEnd(4) +", " +xEnd(5) +"]")
                     disp("CovMatKF = " + CovMatKF(1,1) + ", " + CovMatKF(1,2) + ", "+ CovMatKF(2,2) )
                     disp("CovMat = " + CovMat)
                     disp("sigma^2 = " + sigma^2)
                     llLNATrans = -1.e10;
                     %pause
                end
                
                
                %storing ll values for this measurement
                llLNAn = llLNAn + llLNATrans;

        end

        %storing ll values for this trajectory
        llLNAnAll(k) = llLNAn;
    end
    llNew = sum(llLNAnAll);
end

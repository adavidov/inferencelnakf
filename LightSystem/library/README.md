# inferenceLNAKF

## LyghtSystem/library

Here we find all functions needed to run the MCMC search with filtered LNA

Files: 
 1. MCMCSearchGroups.m  - initialisation of the MCMC search, defining the proposal distribution, defining limits on parameters, calling Likelihood function, recording proposed parameters and likelihood evaluation.
 
 2. LikelihoodGroups.m - for a given set of parameters and given data calculates likelihood using filtered LNA. 
 			Call to HillFunctionGroups.m, initialisation of moments equations. Looping through every cell.
 			For every cell: looping through every measurement 
 				On every loop there is a call to KalmanUpdate.m that updates initial state for moments2ndLNAwithHiddenODE.m.
 				Call to moments2ndLNAwithHiddenODE.m that predicts the following state of the cell 
 				Update of the likelihood assuming Gaussian ditribution of the state
 			 	 
 
 3. KalmanUpdate.m  - classical Kalman filter that uses model prediction, observations, and assumption on the measurement noise. Takes and gives back uncentered moments, as needed for moment equations. 
 
 4. moments2ndLNAwithHiddenODE - uncentered moment equations solved over [ti, ti+1] time, based on the initial condition, parameters of the model and light input on the given time. This is a local prediction for the state of the cell at time ti+1 based on the initial state at time ti.
 
 5. HillFunctionGroups.m - solves a simple ODE for the light activation and computes Hill function using model parameters. 
  

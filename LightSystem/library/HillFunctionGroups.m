% Hill function 
function [Ht, Lt] = HillFunctionGroups(measurestep,Tmax,uIn,c2,nH,Kd,H0)
    options = odeset('RelTol',1e-10);
    
    tspan = 0:Tmax;
    y0 = H0;
    
    nTime = length(tspan);
    y_list = zeros(nTime, length(y0));
    y_list(1,:) = y0;
    light = 0;
    i = 0;
    
    for iTime = 2:nTime   
        [t_,y_] = ode45(@(t,y) light-c2*y,tspan(iTime-1:iTime),y0,options);
        if ( mod(iTime,measurestep) == 0 ) 
            i = i+1;
            light = uIn(i);
        end
        y0 = y_(end, :);
        y_list(iTime, :) = y0;
    end
    
    Ht = y_list;
    nom = (Ht*c2).^nH;
    den = Kd.^nH + nom;
    Lt = nom./den;
    
end


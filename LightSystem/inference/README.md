# inferenceLNAKF

## LyghtSystem/inference

Files < input*.m >  are Matlab scripts. In each of these we :
 - select datafile to work on (experimental or simulated)
 - define file to save MCMC search results 
 - define length of the MCMC search and other parameters needed for the search that we read from the datafile
 - run the MCMC search
 
 For every input file there is a corresponding .mat file with saved run of the MCMC search. 
 For simulated data there is a change of names in files. 
 For example when we perform a parameter inference using datafile: 1021x12-t0-allGroups.mat , 
 the resulting MCMC chain is saved in 1021x12-t0-llchain-G0S1-irngrand.mat
 
 Simlarly other files names get changes as in the following table: 
 
 data       MCMC 
 ----------------
 allGroups  G0
 Group2     G1
 Group3     G2
 Group6     G5
 Group7     G6
 
 The reason for this is that in the provided data Group1 is a trivial group without any green light signal,
 that we never use for parameter inference. For the sake of readability in one moment we decided to change names
 of the groups and start numbering groups used for inference from 1. 
 

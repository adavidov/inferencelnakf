clear 
close all

addpath('~/LightSystem/inference/')
addpath('~/LightSystem/data/synthetic/')
addpath('~/LightSystem/library/')

% starting parameters
% paras = [c2, a, b, s, muE, nH, kappa, h2]

rng('shuffle');
initialParas = [rand(), rand(),  rand(), 5*rand(), 100*rand(),  5*rand(), rand(), rand()]

load ~/LightSystem/data/synthetic/1021x30-t0-Group7.mat

% choose chain legth
ChainLength = 10000;

%set name to save results accordingly
filename_llchain = sprintf('~/LightSystem/inference/1021x30-t0-llchain-G6S1-irngrand.mat');
disp("files names set")


    %number of groups read from the data
    G = size(Y_all,2);
    if G == 1
	uIn = uIn_comp;
    end
    %number of measurements, read from the data
    numberOfMeasurements = size(Y_all{1},2);

    %number of cells for each group is saved in numCellsAllGroups 
    numCellsAllGroups = zeros(1,G);
    for g = 1:G
        numCellsAllGroups(g) = size(Y_all{g},1);
    end

    % total number of cells
    numtraj = sum(numCellsAllGroups); 

    %cummulative number of cells
    cumnumCells = cumsum(numCellsAllGroups);

    % time cycle at which we measure state of the system (cell)
    measurestep = 6;

    % total time of one cell tracking,  240 * 6  = 1440 minuta
    Tmax = measurestep*numberOfMeasurements; 


    % estimate of noise in the signal for each group. It also can be 
    % sigma = constant.
    S_all = cell(1,G);
    for g = 1:G
        S_all{g} = ones(1,numberOfMeasurements);
    end


disp("Data parameters and noise model set.")


% set numebr of processors for matlab code to use
numProc = 30;

MCMCSearchGroups(filename_llchain, numProc, ChainLength, initialParas, G, numberOfMeasurements, numtraj, cumnumCells, measurestep, Tmax, uIn, Y_all, S_all);




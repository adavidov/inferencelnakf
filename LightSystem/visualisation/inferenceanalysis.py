#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May  9 20:19:14 2020

@author: andjela
"""

# 1021x12-t0-llchain-G0S1-irngrand.mat
# 1021x12-t0-llchain-G2S1-irngrand.mat
# 1021x12-t0-llchain-G3S1-irngrand.mat
# 1021x12-t0-llchain-G6S1-irngrand.mat
# 1021x12-t0-llchain-G7S1-irngrand.mat

# 1021x30-t0-llchain-G0S1-irngrand.mat
# 1021x30-t0-llchain-G2S1-irngrand.mat
# 1021x30-t0-llchain-G3S1-irngrand.mat
# 1021x30-t0-llchain-G6S1-irngrand.mat
# 1021x30-t0-llchain-G7S1-irngrand.mat

# 1021x6-t0-llchain-G0S1-irngrand.mat
# 1021x6-t0-llchain-G2S1-irngrand.mat
# 1021x6-t0-llchain-G3S1-irngrand.mat
# 1021x6-t0-llchain-G6S1-irngrand.mat
# 1021x6-t0-llchain-G7S1-irngrand.mat
#
# 1021x60-llchain-G0S1-irngrand.mat
# 1021x60-llchain-G2S1-irngrand.mat
# 1021x60-llchain-G3S1-irngrand.mat
# 1021x60-llchain-G6S1-irngrand.mat
# 1021x60-llchain-G7S1-irngrand.mat

# 1021x60-llchain-old-G0S1-irngrand.mat
# 1021x60-llchain-old-G2S1-irngrand.mat
# 1021x60-llchain-old-G3S1-irngrand.mat
# 1021x60-llchain-old-G6S1-irngrand.mat
# 1021x60-llchain-old-G7S1-irngrand.mat

# dataAll30-llchain-3.mat
# dataAll30-llchain-allGS1-2.mat
# dataAll30-llchain-allGS1-4.mat
# dataAll30-llchain-allGS1.mat
# dataAll30-llchain-allGS1-tot.mat
# dataAll30-llchain.mat
# dataAllGroups-llchain.mat

# 6, 12, 30
# data/synthetic/1021x6-t0-Group3.mat
# data/synthetic/1021x6-t0-allGroups.mat
# data/synthetic/1021x6-t0-Group7.mat
# data/synthetic/1021x6-t0-Group6.mat
# data/synthetic/1021x6-t0-Group2.mat

# 60
# data/synthetic/1021x60-new-allGroups.mat
# data/synthetic/1021x60-new-Group2.mat
# data/synthetic/1021x60-new-Group7.mat
# data/synthetic/1021x60-new-Group3.mat
# data/synthetic/1021x60-new-Group6.mat


import os
os.chdir('/home/andjela/work/projects/inbio/src/lightsystem-python/')
import scipy.io
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pathlib import Path
from matplotlib import ticker, cm
import liblight as ll
import importlib
import math
importlib.reload(ll)

SMALL_SIZE = 12
MEDIUM_SIZE = 14
BIGGER_SIZE = 16

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

filesavebase = '/home/andjela/work/projects/inbio/src/lightsystem-python/figures/'
p_def = {'a': 0.6228, 'b': 0.0113, 's': 1.5631, 'm': 4.1176,
         'h': 0.0059, 'c': 0.0842, 'n': 1.4312, 'k': 0.5818}

# %%
'''
    Compare different groups for the same cell number
'''
importlib.reload(ll)
cellnumber = 12
fbase = f'{cellnumber}-allgroups'
groups = [0, 1, 2, 5, 6]
plt.figure(figsize=(8, 12))
for i,group in enumerate(groups):
    openfile = f'1021x{cellnumber}-t0-llchain-G{group}S1-irngrand'
    params, lls, initParas = ll.open_matlab_mcmc_file(openfile)
    burnin = 4000
    params = params[:, burnin:]
    lls = lls[burnin:]
    ll.plot_densities(params, labelname=f'G{group}', color=f'C{i}',exact=p_def)
plt.savefig(f'{filesavebase}{fbase}-densities.pdf')

# %%
'''
    Compare 12 vs 30 vs 60 cells for the same group
'''
importlib.reload(ll)
group = 6
cellnumbers = [12, 30, 60]
fbase = f'G{group}-12-30-60-cells'
plt.figure(figsize=(8, 12))
for cellnumber in cellnumbers:
    openfile = f'1021x{cellnumber}-t0-llchain-G{group}S1-irngrand'
    params, lls, initParas = ll.open_matlab_mcmc_file(openfile)
    burnin = 4000
    params = params[:, burnin:]
    lls = lls[burnin:]
    ll.plot_densities(params, labelname=f'{cellnumber} cells', exact=p_def)
plt.savefig(f'{filesavebase}{fbase}-densities.pdf')


# %%
'''
    Compare 30 vs 60 cells for G5, G5
'''
importlib.reload(ll)
cellnumbers = [30, 60]
groups = [0, 1, 2, 5, 6]
for group in groups:
    plt.figure(figsize=(8, 12))
    fbase = f'G{group}-30-60-cells'
    for cellnumber in cellnumbers:
        openfile = f'1021x{cellnumber}-t0-llchain-G{group}S1-irngrand'
        params, lls, initParas = ll.open_matlab_mcmc_file(openfile)
        burnin = 4000
        params = params[:, burnin:]
        lls = lls[burnin:]
        ll.plot_densities(params, labelname=f'{cellnumber} cells')
    plt.savefig(f'{filesavebase}{fbase}-densities.pdf')
# %%
'''
    Compare 12 vs 30 vs 60 cells for G5 scatter
'''
importlib.reload(ll)
cellnumbers = [12, 30, 60]
group = 6
fbase = f'G{group}-12-30-60-cells'
plt.figure(figsize=(16, 15))
for cellnumber in cellnumbers:
    openfile = f'1021x{cellnumber}-t0-llchain-G{group}S1-irngrand'
    params, lls, initParas = ll.open_matlab_mcmc_file(openfile)
    burnin = 4000
    params = params[:, burnin:]
    lls = lls[burnin:]
    ll.plot_scatter(params)
plt.savefig(f'{filesavebase}{fbase}-scatter.pdf')
plt.savefig(f'{filesavebase}{fbase}-scatter.png')



# %%
'''
    Plot means for data + model -predicted means for G0
'''
importlib.reload(ll)
data_type = 'synthetic'
cellnumbers = [12, 30, 60]
groups = [0, 1, 2, 5, 6]

for numcells in cellnumbers:
    for group in groups:
        fbase = f'G{group}-cells-{numcells}'
        if data_type == 'synthetic':
            if numcells == 60:
                fnamestart = f'synthetic/1021x{numcells}-'
            else:
                fnamestart = f'synthetic/1021x{numcells}-t0-'
            if group == 0:
                fnameend = 'allGroups'
            else:
                fnameend = f'Group{group+1}'        
        data = ll.open_matlab_data_file(fnamestart+fnameend)

        ng = data["numgroups"]
        ntg = int(data["numtraj"]/data["numgroups"])
        mstep = data["measurestep"]
        if ng > 1:
            plt.figure(figsize=[14, 9])
        else:
            plt.figure(figsize=[8, 4])

        for g in range(ng):
            # this is necessarry due to the way we saved data in matlab
            light = np.append([0], data["light"][g][0:-1])
            # compute mean of data
            mean_data = np.mean(data["F"][g], axis=0)
            # take all trajectories of this group
            traj = [tr for tr in data["F"][g]]
            # plot all trajectories
            times = np.arange(0, len(light))
            if ng > 1:
                plt.subplot(ng/2, 2, g+1)
            plt.xticks([])
            for i in range(len(traj)):
                plt.plot(times, traj[i], 'k', alpha=0.2, lw=1)
            # plot mean of trajectories
            plt.plot(times, mean_data, 'crimson', lw=2, label='data mean')
            # exact parameters mean prediction, no use of the last input
            # inside this function the initial data and light function
            # is defined to 0
            exact_model_moments = ll.solve_uncentered_moments_LNA(mstep,
                                            light[0:-1], params=data["paras"])
            # mean of the prediction
            plt.plot(times, data['paras']['s']*exact_model_moments[:, 2], lw=2,
                     label='ideal mean',
                     color='dodgerblue')
            # variance representation on the same graph as a shade
#            plt.fill_between(
#                times,
#                data['paras']['s']*exact_model_moments[:, 2] +
#                data['paras']['s']*np.sqrt((exact_model_moments[:, 5] -
#                                            exact_model_moments[:, 2]**2)),
#                data['paras']['s']*exact_model_moments[:, 2] -
#                data['paras']['s']*np.sqrt((exact_model_moments[:, 5] -
#                                            exact_model_moments[:, 2]**2)),
#                color='dodgerblue', alpha=0.2
#                )
            # light function solution, without delay in time
            # plt.plot(times, exact_model_moments[:, 0], lw=2,
            #         label='light system',
            #         color='darkgreen')
            # light signal under the main graph
            locs, labels = plt.yticks()
            y_val = -0.01*max(locs)
            bw = max(locs)*0.05
            y1 = y_val-bw
            y2 = y_val-bw-bw*light 
            plt.fill_between(times, y1, y2, where=y1>y2, color='darkgreen', alpha=.5)
            y3 = y_val-2*bw
            y4 = y_val-2*bw-bw*(1-light)
            plt.fill_between(times, y3, y4, where=y3>y4, color='crimson', alpha=.5)

            #plt.plot(times, (-2+light)*max(mean_data)*0.3, lw=2,
                     # label='light signal',
             #        color='seagreen')
            # setting ticks
            locs, labels = plt.yticks()
            plt.yticks(np.append(np.arange(0, locs[-1]+1, step=locs[-1]/2), y_val-2*bw), np.append(np.arange(0, locs[-1]+1, step=locs[-1]/2), "signal"))
            # setting y label
            if ng > 1:
                plt.ylabel(f'fluorescence')
                if g == 4 or g == 5:
                    plt.xticks(np.arange(0, len(traj[0])+1, step=40),
                               np.arange(0, mstep*(len(traj[0])+1), step=6*40))
                    plt.xlabel(f'time (min)')
                if g == 1:
                    plt.legend(loc='upper right')
            else:
                plt.ylabel(f'fluorescence')
                plt.xticks(np.arange(0, len(traj[0])+1, step=40),
                           np.arange(0, mstep*(len(traj[0])+1), step=6*40))
                plt.xlabel(f'time (min)')
                #plt.legend(loc='upper left')
        plt.show()
        plt.savefig(f'{filesavebase}{fbase}-data-mean.pdf', bbox_inches="tight")

# %%
plt.close('all')

# %%
'''
    Plot means for data + model predicted means using MLE from these data MCMC
'''
importlib.reload(ll)
# choose data 
data_type = 'synthetic'
cellnumbers = [60]
groups = [0, 1, 2, 5, 6]
for numcells in cellnumbers:
    for group in groups:
        fbase = f'G{group}-cells-{numcells}'
        # open data file
        data = ll.open_matlab_data_file(ll.matlab_data_pattern(data_type, group,
                                                            numcells))
        # find number of groups
        ng = data["numgroups"]
        # compute number of trajectories per group
        ntg = int(data["numtraj"]/data["numgroups"])
        # take measurestep
        mstep = data["measurestep"]
        # start new figure for each dataset
        if ng > 1:
            plt.figure(figsize=[14, 9])
        else:
            plt.figure(figsize=[7, 3])
        for g in range(ng):
            # this is necessarry due to the way we saved data in matlab
            light = np.append([0], data["light"][g][0:-1])
            # compute mean of data
            mean_data = np.mean(data["F"][g], axis=0)
            # take all trajectories of this group
            traj = [tr for tr in data["F"][g]]
            # plot all trajectories
            times = np.arange(0, len(light))
            if ng > 1:
                plt.subplot(ng/2, 2, g+1)
            plt.xticks([])
            for i in range(len(traj)):
                plt.plot(times, traj[i], 'k', alpha=0.2, lw=1)
            # plot mean of trajectories
            plt.plot(times, mean_data, 'crimson', lw=2, label='data mean')
            
            # exact parameters mean prediction, no use of the last input
            # inside this function the initial data and light function
            # is defined to 0
            exact_model_moments = ll.solve_uncentered_moments_LNA(mstep, light[0:-1], params=data["paras"])
            # mean of the prediction with exact parameters
            plt.plot(times, data['paras']['s']*exact_model_moments[:, 2], lw=2,
                     label='ideal mean',
                     color='dodgerblue')

            # Now, find maximum likelihood estimator from their MCMC chain
            openfile = f'1021x{numcells}-t0-llchain-G{group}S1-irngrand'
            params, lls, initParas = ll.open_matlab_mcmc_file(openfile)
            mle_params = ll.mle(params, lls)
            print(g+1) if ng>1 else print(group)
            print(f"{mle_params['a']:6.4f} & {mle_params['b']:6.4f} & {mle_params['s']:6.4f} & {mle_params['m']:6.4f} & {mle_params['h']:6.4f} & {mle_params['c']:6.4f} & {mle_params['n']:6.4f} & {mle_params['k']:6.4f} \\\\")
            model_moments = ll.solve_uncentered_moments_LNA(mstep, light[0:-1], params=mle_params)
            # mean of the prediction with learnt parameters
            plt.plot(times, mle_params['s']*model_moments[:, 2],
                      lw=2,
                      label=f'mean prediction',
                      color='orange'
                      )
            # light signal under the main graph     
            locs, labels = plt.yticks()
            y_val = -0.01*max(locs)
            bw = max(locs)*0.05
            y1 = y_val-bw
            y2 = y_val-bw-bw*light 
            plt.fill_between(times, y1, y2, where=y1>y2, color='darkgreen', alpha=.5)
            y3 = y_val-2*bw
            y4 = y_val-2*bw-bw*(1-light)
            plt.fill_between(times, y3, y4, where=y3>y4, color='crimson', alpha=.5)
             # setting ticks
            locs, labels = plt.yticks()
            plt.yticks(np.append(np.arange(0, locs[-1]+1, step=locs[-1]/2), y_val-2*bw), np.append(np.arange(0, locs[-1]+1, step=locs[-1]/2), "signal"))
          
            # plt.plot(times, (-2+light)*max(mean_data)*0.3, lw=2,
            #          # label='light signal',
            #          color='seagreen')
            # # setting ticks
            # locs, labels = plt.yticks()
            # plt.yticks(np.arange(0, locs[-1]+1, step=locs[-1]/2))
            # setting y label
            if ng > 1:
                plt.ylabel(f'fluorescence')
                if g == 4 or g == 5:
                    plt.xticks(np.arange(0, len(traj[0])+1, step=40),
                               np.arange(0, mstep*(len(traj[0])+1), step=6*40))
                    plt.xlabel(f'time (min)')
                if g == 1:
                    plt.legend(loc='upper right')
            else:
                plt.ylabel(f'fluorescence')
                plt.xticks(np.arange(0, len(traj[0])+1, step=40),
                           np.arange(0, mstep*(len(traj[0])+1), step=6*40))
                plt.xlabel(f'time (min)')
                # plt.legend(loc='upper right')
#        plt.show()
        plt.savefig(f'{filesavebase}{fbase}-sim-exact-mle.pdf',bbox_inches="tight")

# %%
plt.close('all')

# %%
'''
    Plot means for data + model predicted means using MLE from different MCMC
'''

importlib.reload(ll)
# choose data 
data_type = 'synthetic'
cellnumbers = [60]
groups = [0, 1, 2, 5, 6]
# choose mle estiamtors 
grs = [0, 1, 2, 5, 6]
for cn in [60]:
    for numcells in cellnumbers:
        for group in groups:
            fbase = f'G{group}-cells-{numcells}-with-MLE-cn-{cn}'
            # open data file
            data = ll.open_matlab_data_file(ll.matlab_data_pattern(data_type, group,
                                                                numcells))
            # find number of groups
            ng = data["numgroups"]
            # compute number of trajectories per group
            ntg = int(data["numtraj"]/data["numgroups"])
            # take measurestep
            mstep = data["measurestep"]
            # start new figure for each dataset
            if ng > 1:
                plt.figure(figsize=[14, 9])
            else:
                plt.figure(figsize=[7, 3])
            for g in range(ng):
                # this is necessarry due to the way we saved data in matlab
                light = np.append([0], data["light"][g][0:-1])
                # compute mean of data
                mean_data = np.mean(data["F"][g], axis=0)
                # take all trajectories of this group
                traj = [tr for tr in data["F"][g]]
                # plot all trajectories
                times = np.arange(0, len(light))
                if ng > 1:
                    plt.subplot(ng/2, 2, g+1)
                plt.xticks([])
    #            for i in range(len(traj)):
    #                plt.plot(times, traj[i], 'k', alpha=0.2)
                # plot mean of trajectories
                plt.plot(times, mean_data, 'crimson', lw=2, label='data mean')
                
                
                
                # exact parameters mean prediction, no use of the last input
                # inside this function the initial data and light function
                # is defined to 0
                exact_model_moments = ll.solve_uncentered_moments_LNA(mstep, light[0:-1], params=data["paras"])
                # mean of the prediction with exact parameters
                plt.plot(times, data['paras']['s']*exact_model_moments[:, 2], lw=2,
                         label='true mean',
                         color='dodgerblue')
    
                # light function solution, without delay in time
                # plt.plot(times, exact_model_moments[:, 0], lw=2,
                #         label='light system',
                #         color='darkgreen')
    
                # Now, find maximum likelihood estimator from some MCMC chain
                for i, gr in enumerate(grs):
                    openfile = f'1021x{cn}-t0-llchain-G{gr}S1-irngrand'
                    params, lls, initParas = ll.open_matlab_mcmc_file(openfile)
                    mle_params = ll.mle(params, lls)
                    model_moments = ll.solve_uncentered_moments_LNA(mstep, light[0:-1], params=mle_params)
                    # mean of the prediction with learnt parameters
                    plt.plot(times, mle_params['s']*model_moments[:, 2],
                             lw=2,
                             label=f'MAP: G{gr}, 60 cells',
                             alpha=0.7
                             )
                    
                # light signal under the main graph
                #plt.plot(times, (-2+light)*max(mean_data)*0.3, lw=2,
                         # label='light signal',
                #         color='seagreen')
                locs, labels = plt.yticks()
                y_val = -0.01*max(locs)
                bw = max(locs)*0.05
                y1 = y_val-bw
                y2 = y_val-bw-bw*light 
                plt.fill_between(times, y1, y2, where=y1>y2, color='darkgreen', alpha=.5)
                y3 = y_val-2*bw
                y4 = y_val-2*bw-bw*(1-light)
                plt.fill_between(times, y3, y4, where=y3>y4, color='crimson', alpha=.5)
                # setting ticks
                # locs, labels = plt.yticks()
                # plt.yticks(np.arange(0, locs[-1]+1, step=locs[-1]/2))
                # setting ticks
                locs, labels = plt.yticks()
                plt.yticks(np.append(np.arange(0, locs[-1]+1, step=locs[-1]/2), y_val-2*bw), np.append(np.arange(0, locs[-1]+1, step=locs[-1]/2), "signal"))
          
                # setting y label
                if ng > 1:
                    plt.ylabel(f'fluorescence')
                    if g == 4 or g == 5:
                        plt.xticks(np.arange(0, len(traj[0])+1, step=40),
                                   np.arange(0, mstep*(len(traj[0])+1), step=6*40))
                        plt.xlabel(f'time (min)')
                    if g == 1:
                        plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
                else:
                    plt.ylabel(f'fluorescence')
                    plt.xticks(np.arange(0, len(traj[0])+1, step=40),
                               np.arange(0, mstep*(len(traj[0])+1), step=6*40))
                    plt.xlabel(f'time (min)')
                    # plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
            plt.show()
            plt.savefig(f'{filesavebase}{fbase}-means.pdf', bbox_inches="tight")

# %%
plt.close('all')


# %%
'''
    Plot means for data python simulated + model -predicted means
'''
importlib.reload(ll)
data_type = 'synthetic'
numcells = 12
group = 5
fbase = f'G{group}-cells-{numcells}'
if data_type == 'synthetic':
    if numcells == 60:
        fnamestart = f'synthetic/1021x{numcells}-'
    else:
        fnamestart = f'synthetic/1021x{numcells}-t0-'
    if group == 0:
        fnameend = 'allGroups'
    else:
        fnameend = f'Group{group+1}'        
pattern = fnamestart+fnameend

matlab_data = ll.open_matlab_data_file(pattern)

# %%
python_data = ll.simulate_group_like_in_matlab(pattern)
data = matlab_data

# %%
'''
    Real data
'''
importlib.reload(ll)
fbase = 'realG0-30-fixed'

openfile = f'dataAll30-llchain-fixedparas'
params, lls, initParas = ll.open_matlab_mcmc_file(openfile)
burnin = 4000
params = params[:, burnin:]
lls = lls[burnin:]

cs = params[5, :] < 15
params = params[:, cs]
lls = lls[cs]

#%%
mle_params = ll.mle(params, lls)
ll.plot_mcmc_chain_fixedpar(params, lls, mle=mle_params)
plt.savefig(f'{filesavebase}{fbase}-chain.pdf')
plt.show()

#%%
ll.plot_densities_fixedpar(params, mle=mle_params)
plt.savefig(f'{filesavebase}{fbase}-densities.pdf')
plt.show()

# %%
plt.figure(figsize=(10, 8))
ll.plot_scatter_fixedpar(params, exact=mle_params)
plt.savefig(f'{filesavebase}{fbase}-scatter.pdf')
plt.savefig(f'{filesavebase}{fbase}-scatter.png')


# %%
'''
    Real data OLD
'''
importlib.reload(ll)
fbase = 'realG0-30-old'

openfile = f'dataAll30-llchain-allGS1-2020'
params, lls, initParas = ll.open_matlab_mcmc_file(openfile)
burnin = 10000
end =40000
params = params[:, burnin:end]
lls = lls[burnin:end]

#%%
mle_params = ll.mle(params, lls, burnin = 0)
ll.plot_mcmc_chain(params, lls, mle=mle_params)
plt.savefig(f'{filesavebase}{fbase}-chain.pdf')
plt.show()

# % 2020
# % 'a': 8.6639376184110741,
# % 'b': 0.0080293915240137563,
# % 's': 4.1638682980886834,
# % 'm': 0.063310112387652745,
# % 'h': 17.562064285793387,
# % 'c': 3.1086605957084292,
# % 'n': 9.192180210396657,
# % 'k': 0.2707589759916546
# %initialParas = [3.1086605957084292,8.6639376184110741,0.0080293915240137563,4.1638682980886834,0.063310112387652745,9.192180210396657,0.2707589759916546,17.562064285793387]


#%%
ll.plot_densities(params, mle=mle_params)
plt.savefig(f'{filesavebase}{fbase}-densities.pdf')
plt.show()

# %%
plt.figure(figsize=(10, 8))
ll.plot_scatter(params, exact=mle_params)
plt.savefig(f'{filesavebase}{fbase}-scatter.pdf')
plt.savefig(f'{filesavebase}{fbase}-scatter.png')

#%%

cs = params[5, :] < 15
params = params[:, cs]
lls = lls[cs]



# %%
'''
    Real data limits
'''

importlib.reload(ll)
fbase = 'realG0-30'
openfile = f'dataAll30-llchain-limits-0'
params, lls, initParas = ll.open_matlab_mcmc_file(openfile)
burnin = 5000
params = params[:, burnin:]
lls = lls[burnin:]



openfile = f'dataAll30-llchain-limits-1'
params1, lls1, initParas1 = ll.open_matlab_mcmc_file(openfile)
burnin = 0
params1 = params1[:, burnin:]
lls1 = lls1[burnin:]

params = np.concatenate((params, params1), axis=1)
lls = np.concatenate((lls, lls1))


# openfile = f'dataAll30-llchain-limits-2'
# params2, lls2, initParas2 = ll.open_matlab_mcmc_file(openfile)
# burnin = 0
# params2 = params2[:, burnin:]
# lls2 = lls2[burnin:]

# params = np.concatenate((params, params2), axis=1)
# lls = np.concatenate((lls, lls2))


# openfile = f'dataAll30-llchain-limits-3'
# params3, lls3, initParas3 = ll.open_matlab_mcmc_file(openfile)
# burnin = 0
# params3 = params3[:, burnin:]
# lls3 = lls3[burnin:]

# params = np.concatenate((params, params3), axis=1)
# lls = np.concatenate((lls, lls3))

#%%
mle_params = ll.mle(params, lls)
ll.plot_mcmc_chain(params, lls, mle=mle_params)
plt.savefig(f'{filesavebase}{fbase}-chain.pdf')

#%%
ll.plot_densities(params, mle=mle_params)
plt.savefig(f'{filesavebase}{fbase}-densities.pdf')

# %%
ll.plot_scatter(params, exact=mle_params)
plt.savefig(f'{filesavebase}{fbase}-scatter.pdf')
plt.savefig(f'{filesavebase}{fbase}-scatter.png')


# %%
'''
    Real data limits 2021
'''

importlib.reload(ll)
fbase = 'realG0-2021'
openfile = f'dataAll30-llchain-limits-2021.mat'
params, lls, initParas = ll.open_matlab_mcmc_file(openfile)
burnin = 40000
end = 50000
params = params[:, burnin:end]
lls = lls[burnin:end]

openfile = f'dataAll30-llchain-limits-2021-1.mat'
params1, lls1, initParas1 = ll.open_matlab_mcmc_file(openfile)
burnin = 0
end = 50000
params1 = params1[:, burnin:end]
lls1 = lls1[burnin:end]

params = np.concatenate((params, params1), axis=1)
lls = np.concatenate((lls, lls1))

#%%
mle_params = ll.mle(params, lls)
ll.plot_mcmc_chain(params, lls, mle=mle_params)
plt.savefig(f'{filesavebase}{fbase}-chain.pdf')

#%%
plt.figure(figsize=(8, 15))
ll.plot_densities(params, mle=mle_params)
plt.savefig(f'{filesavebase}{fbase}-densities.pdf')

# %%
plt.figure(figsize=(16, 15))
ll.plot_scatter(params, exact=mle_params)
plt.savefig(f'{filesavebase}{fbase}-scatter.pdf')
plt.savefig(f'{filesavebase}{fbase}-scatter.png')


# %%
'''
    Real data fixed 2021
'''

importlib.reload(ll)
fbase = 'realG0-2021-fixed'
openfile = f'dataAll30-llchain-fixed-2021-1.mat'
params, lls, initParas = ll.open_matlab_mcmc_file(openfile)
burnin = 3000
end = 14000
params = params[:, burnin:end]
lls = lls[burnin:end]

#%%
mle_params = ll.mle(params, lls)
ll.plot_mcmc_chain_fixedpar(params, lls, mle=mle_params)
plt.savefig(f'{filesavebase}{fbase}-chain.pdf')

#%%
ll.plot_densities_fixedpar(params, mle=mle_params, factorh=False)
plt.savefig(f'{filesavebase}{fbase}-densities.pdf')

# %%
ll.plot_scatter_fixedpar(params, exact=mle_params)
plt.savefig(f'{filesavebase}{fbase}-scatter.pdf')
plt.savefig(f'{filesavebase}{fbase}-scatter.png')

# %%
plt.close('all')


# %%
'''
    Real data error 2021
'''

importlib.reload(ll)
fbase = 'realG0-2021-error'
openfile = f'dataAll30-llchain-fixed2-2021-withmistake'
params, lls, initParas = ll.open_matlab_mcmc_file(openfile)
burnin = 0
end = 2000
params = params[:, burnin:end]
lls = lls[burnin:end]

#%%
mle_params = ll.mle(params, lls)
ll.plot_mcmc_chain_fixedpar(params, lls, mle=mle_params)
plt.savefig(f'{filesavebase}{fbase}-chain.pdf')

#%%
ll.plot_densities_fixedpar(params, mle=mle_params, factorh=False)
plt.savefig(f'{filesavebase}{fbase}-densities.pdf')

# %%
ll.plot_scatter_fixedpar(params, exact=mle_params)
plt.savefig(f'{filesavebase}{fbase}-scatter.pdf')
plt.savefig(f'{filesavebase}{fbase}-scatter.png')

# %%
plt.close('all')

# %%
'''
    Real data 5G fixed paras
'''
importlib.reload(ll)
openfile = f'dataG0-10'
fbase = 'real5G-G0-10'
params, lls, initParas = ll.open_matlab_mcmc_file(openfile)
burnin = 3000
params = params[:, burnin:]
lls = lls[burnin:]


#%%
mle_params = ll.mle(params, lls)
ll.plot_mcmc_chain_fixedpar(params, lls, mle=mle_params)
plt.savefig(f'{filesavebase}{fbase}-chain.pdf')


#%%
ll.plot_densities_fixedpar(params, mle=mle_params, factorh=True)
plt.savefig(f'{filesavebase}{fbase}-densities.pdf')



# %%
#data = ll.open_matlab_data_file('real/'+openfile)
data = ll.open_matlab_data_file('real/dataAll30')

# find number of groups
ng = data["numgroups"]
# compute number of trajectories per group
ntg = len(data["F"][0])
# take measurestep
mstep = 6
# start new figure for each dataset
# %%

if ng > 1:
    plt.figure(figsize=[14, 9])
else:
    plt.figure(figsize=[8, 5])
for g in range(ng):
    # this is necessarry due to the way we saved data in matlab
    light = np.append([0], data["light"][g][0:-1])
    # compute mean of data
    mean_data = np.mean(data["F"][g], axis=0)
    # take all trajectories of this group
    traj = [tr for tr in data["F"][g]]
    # plot all trajectories
    times = np.arange(0, len(light))
    if ng > 1:
        plt.subplot(3, 2, g+1)
        plt.ylabel(f'fluorescence')
    if g not in np.array([4,5]):
        plt.xticks([])
    else:
        plt.xticks(np.arange(0, len(traj[0])+1, step=40),
                   np.arange(0, mstep*(len(traj[0])+1), step=6*40))
        plt.xlabel(f'time (min)')
    for i in range(len(traj)):
        plt.plot(times, traj[i], 'k', alpha=0.2)
    # plot mean of trajectories
    plt.plot(times, mean_data, 'crimson', lw=2, label='data mean')
    # exact parameters mean prediction, no use of the last input
    # inside this function the initial data and light function
    # is defined to 0
    print(f"{g} tic")
    model_moments = ll.solve_uncentered_moments_LNA(mstep, light[0:-1], params=mle_params)
#    print(f"{g} toc")
#    # mean of the prediction with exact parameters
    plt.plot(times, mle_params['s']*model_moments[:, 2], lw=2,
             label='MAP params',
             color='dodgerblue')
    if g == 1:
        plt.legend(loc='upper right')
    # light signal under the main graph
    locs, labels = plt.yticks()
    y_val = -0.01*max(locs)
    bw = max(locs)*0.05
    y1 = y_val-bw
    y2 = y_val-bw-bw*light 
    plt.fill_between(times, y1, y2, where=y1>y2, color='darkgreen', alpha=.5)
    y3 = y_val-2*bw
    y4 = y_val-2*bw-bw*(1-light)
    plt.fill_between(times, y3, y4, where=y3>y4, color='crimson', alpha=.5)
    # plt.plot(times, (-2+light)*max(mean_data)*0.3, lw=2,
    #          # label='light signal',
    #          color='seagreen')
    # setting ticks
    #locs, labels = plt.yticks()
    #plt.yticks(np.arange(0, locs[-1]+1, step=locs[-1]/2))
    plt.yticks(np.append(np.arange(0, locs[-1]+1, step=locs[-1]/2), y_val-2*bw), np.append(np.arange(0, locs[-1]+1, step=locs[-1]/2), "signal"))
          


plt.show()
plt.savefig(f'{filesavebase}{fbase}-data.pdf')

# %%
plt.close('all')




# %%
'''
    Compare different groups for the same cell number 10
'''
importlib.reload(ll)
cellnumber = 10
fbase = f'dataGx-{cellnumber}-allgroups'
groups = [0,1,2,3,4,5]
plt.figure(figsize=(8, 10))
for i,group in enumerate(groups):
    print(i, group)
    openfile = f'realG{group}-10'
    params, lls, initParas = ll.open_matlab_mcmc_file(openfile)
    burnin = 3000
    params = params[:, burnin:]
    lls = lls[burnin:]
    mle_params = ll.mle(params, lls)
    ll.plot_densities_fixedpar(params, mle=mle_params, labelname=f'G{group}', color=f'C{i}',factorh=False)
plt.savefig(f'{filesavebase}{fbase}-densities.pdf')

# %%
'''
    Compare group 0 for the cell number 10 vs 30
'''
importlib.reload(ll)
fbase = f'realG0-10vs30'

plt.figure(figsize=(8, 10))

openfile = f'dataG0-10'
params, lls, initParas = ll.open_matlab_mcmc_file(openfile)
burnin = 3000
params = params[:, burnin:]
lls = lls[burnin:]
mle_params = ll.mle(params, lls)
ll.plot_densities_fixedpar(params, mle=mle_params, labelname=f'G0-10', color=f'C{1}', factorh=False)
    
openfile = f'dataAll30-llchain-fixedparas-1'
params, lls, initParas = ll.open_matlab_mcmc_file(openfile) 
burnin = 5000
params = params[:, burnin:] 
lls = lls[burnin:]
cs = params[5, :] < 15
params = params[:, cs]
lls = lls[cs]
mle_params = ll.mle(params, lls)
ll.plot_densities_fixedpar(params, mle=mle_params, labelname=f'G0-30', color=f'C{2}', factorh=False)

plt.savefig(f'{filesavebase}{fbase}-densities.pdf')



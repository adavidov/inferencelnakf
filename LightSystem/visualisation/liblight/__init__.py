#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May  9 23:07:50 2020

@author: andjela
"""
from scipy.integrate import solve_ivp
import numpy as np
import scipy.io
import matplotlib.pyplot as plt
from pathlib import Path
from matplotlib import ticker, cm
from random import random, uniform, expovariate, randrange
from statistics import mean, median, stdev
import pandas as pd
import seaborn as sns

p_def = {'a': 0.6228, 'b': 0.0113, 's': 1.5631, 'm': 4.1176,
         'h': 0.0059, 'c': 0.0842, 'n': 1.4312, 'k': 0.5818}

def uncentered_moments_LNA(t, y, p, u):
    '''
    0 --h1-->  E --h2--> 0
    0 --a*E(t)*L(t)--> F --b--> 0
    L(t) = ( c*l(t))^nh / (k^nh + (c*l(t))^nh)
    d/dt l(t) = u(t) - c*l(t)

    '''
   
    l, e, f, ee, ef, ff = y # initial condition for this time step
    L = (p['c']*l)**p['n'] / (p['k']**p['n'] + (p['c']*l)**p['n'])
    dldt = u - p['c']*l
    dedt = p['m']*p['h'] - e*p['h']
    dfdt = e*p['a']*L - f*p['b']
    deedt = p['m']*p['h'] + 2*e*p['m']*p['h'] + e*p['h'] - 2*ee*p['h']
    defdt = f*p['m']*p['h'] - ef*p['h'] - ef*p['b'] + ee*p['a']*L
    dffdt = f*p['b'] - 2*ff*p['b'] + e*p['a']*L + 2*ef*p['a']*L
    return [dldt, dedt, dfdt, deedt, defdt, dffdt]


def solve_uncentered_moments_LNA(mstep, light, params):
    '''
    Integrating moment equations with hill function at the same time to find
    model mean for a given light input and parameters
    Args:
        mstep: measurement step - in my case 6 min
        light: pattern of 0s and 1s
        p: parameters of the light model
    Returns:
        sols: array of ... ?
    '''
    y0 = [0, params['m'], 0, params['m']**2, 0, 0]
    sols = [y0]
    for u in light:
        t_span = [0, mstep]
        sol = solve_ivp(uncentered_moments_LNA, t_span, y0, args=(params,u), t_eval=[mstep])
        sols.append(sol.y[:, 0])
        y0 = sol.y[:, 0]
    return np.array(sols)


def hill_function(measurestep, light, p=p_def):
    """Computing Hill function for the given light input and
    light system parameters.
    Args:
        measurestep: in time units, how many time units pass between
        two measurements.
        light: array of 0s and 1s for light switching on and off,
        always start with 0.
        p: parameters of the light system that define hill function

    Returns:
        y: array l(t) for all t in units of time, every 1 min, l(0) = 0
        hill: array L(t) for all t in units of time, every 1 min , L(0) = 0
    """
    tmax = len(light)*measurestep
    ts = np.arange(measurestep) + 1  # take array [1,2,3,..,measurestep]
    y = np.zeros(tmax+1)
    # initial condition y[0] = 0 is assumed, i-th light signal defines
    # dynamics for the timesteps i*measurestep+[1,2,3,..,measurestep]
    for i, u in enumerate(light):
        yts = u/p['c'] - (u/p['c'] - y[i*measurestep])*np.exp(-p['c']*ts)
        # on this interval the signal did not change and we can use the same
        # intial condition y[i*measurestep] and constant signal u
        y[1+i*measurestep:1+(i+1)*measurestep] = yts
    nom = (p['c']*y)**p['n']
    hill = nom/(p['k']**p['n'] + nom)
    return y, hill


def hill_function_t(t, measurestep, light, p=p_def):
    """Computing Hill function  at givent time t for the given light 
    input and light system parameters.
    Args:
        t: time at which we compute
        measurestep: in time units, how many time units pass
        between two measurements.
        light: array of 0s and 1s for light switching on and off,
        always start with 0.
        p: parameters of the light system that define hill function

    Returns:
        y: value l(t) for exact t in units of time, l(0) = 0
        hill: value L(t) for exact t in units of time, L(0) = 0
    """
    if t > len(light)*measurestep:
        print('not enough light signal')
        return
    # find the measurement before time
    mstep_prev = int(np.floor(t/measurestep))
    # compute y until this meaurement
    y = 0
    for u in light[0:mstep_prev]:
        y = u/p['c'] - (u/p['c'] - y)*np.exp(-p['c']*measurestep)
    # if we are in between two measurements compute the exact value
    if t - measurestep*mstep_prev > 0:
        u = light[mstep_prev]
        y = u/p['c'] - (u/p['c'] - y)*np.exp(-p['c']*(t - measurestep*mstep_prev))
    nom = (p['c']*y)**p['n']
    hill = nom/(p['k']**p['n'] + nom)
    return hill


def simulate_ccas_ccar_system(mstep, light,
                              numtraj=1, p=p_def, noise=False, fakea=100):
    """
    Here we simulate data of the model from Nature paper
    0 --h1-->  E --h2--> 0
    0 --a*E(t)*L(t)--> F --b--> 0
    L(t) = ( c*l(t))^nh / (k^nh + (c*l(t))^nh)
    d/dt l(t) = u(t) - c*l(t)
    E(t) is interpreted as the randomly fluctuating  cell  responsiveness to
    green light
    
    h2 - hidden time scale
    h1 - muE*h2
    VarE = muE

    Args:
        params: parameters of the model a b s muE h2 c nh k
        light: array of 0s and 1s for light switching on and off,
               assumed start 0
        T: total time of simulation
        numsim: number of simulations
    Returns:
        E(t) : cell responsiveness, array
        F(t) : fluorescence
    """
    # derived params
    tmax = len(light)*mstep
    if not noise:
        noise = 0
    print(f'Executing simulate_ccas_ccar_system, tmax = {tmax}')
    trajectories = []
    # stoichiometry matrix
    S = np.array([[1, 0], [-1, 0], [0, 1], [0, -1], [0, 0]])
    A = np.zeros(5)
    # for each trajectory
    for traj in range(numtraj):
        print(f'trajectory: {traj}')
        # initial state of the system
        last_t = 0
        last_state = [p['m'], 0]  # E,F
        next_reaction = 4
        states = []
        states.append([last_t,
                       next_reaction,
                       last_state[0],
                       p['s']*last_state[1]])

        # run the algorithm until final time
        while last_t < tmax:
            # calculate propensities a(j) and total propensity a0
            A[0] = p['m']*p['h']
            A[1] = p['h']*last_state[0]
            A[2] = p['a']*last_state[0]*hill_function_t(last_t, mstep, light, p)
            A[3] = p['b']*last_state[1]
            A[4] = fakea
            a0 = np.sum(A)
            dt = expovariate(a0)
            next_reaction = np.argmax(random()*a0 < np.cumsum(A))
            last_state = last_state + S[next_reaction]
            last_t += dt
            states.append([last_t,
                           next_reaction,
                           last_state[0],
                           p['s']*last_state[1] + noise*np.random.randn()])
        print(f'last generated time: {last_t}')
        trajectories.append(np.array(states))
    return trajectories

def simulate_group_like_in_matlab(pattern):
    matlab_data = open_matlab_data_file(pattern)
    
    pyth_data = simulate_ccas_ccar_system(
             matlab_data["measurestep"], 
             matlab_data["light"][0],
             numtraj=matlab_data["numtraj"], 
             p=matlab_data["paras"], 
             noise=matlab_data["sigma"],
             fakea = 10)
    
    times = [traj[:,0] for traj in pyth_data]
    states = [traj[:,2:] for traj in pyth_data]
    E = []
    F = []
    for gr in range(matlab_data["numgroups"]):
        Egr = np.zeros([matlab_data["numtraj"], matlab_data["numberOfMeasurements"]])
        Fgr = np.zeros([matlab_data["numtraj"], matlab_data["numberOfMeasurements"]])
        for traj in range(matlab_data["numtraj"]):
            for itm, tm in enumerate(np.arange(0,matlab_data["Tmax"],matlab_data["measurestep"])):
                if tm == 0:
                    Egr[traj,itm] = states[traj][0][0]
                    Fgr[traj,itm] = states[traj][0][1]
                else:
                    Egr[traj,itm] = states[traj][np.argmin(times[traj] < tm) - 1][0]
                    Fgr[traj,itm] = states[traj][np.argmin(times[traj] < tm) - 1][1]
        E.append(Egr)
        F.append(Fgr)

    
    mat = {'light': matlab_data["light"],  # matrix (ng, numberOfMeasurements)
           'measurestep': matlab_data["measurestep"],  # scalar, most probably 6 min
           'numberOfMeasurements': matlab_data["numberOfMeasurements"],  # scalar
           'numgroups': matlab_data["numgroups"],  # scalar
           'Tmax': matlab_data["Tmax"], # scalar
           'paras': matlab_data["paras"], # dictionary, parameters used in the simulation
           'numtraj': matlab_data["numtraj"], # number of trajectories per group
           'sigma': matlab_data["sigma"],  # noise
           'states': states,  # list of ng*numtraj matreces, each matrix (n_of_states, 2)
           'times': times,  # list of ng*numtraj arrays, each array of length n_of_states
           'E': E,  # list of numgroups matrices, each matrix (numtraj, numberOfMeasurements)
           'F': F,  # list of numgroups matrices, each matrix (numtraj, numberOfMeasurements)
           'E0': matlab_data["E0"],  # scalar
           'F0': matlab_data["F0"]  # scalar
        }
    return mat

def open_matlab_data_file(pattern):
    """Opens matlab files that are used for parameter learning in matlab code.
    Stores data in more consistent way
    Args:
        pattern: part of the filename

    Returns:
        mat dictionary with:
            F:
                observed fluorescence, list of arrays,
                length of the list is the number of groups,
                each array has a shape of
                number of cells x number of measurements
            numgroups:
                number of groups
            light:
                array or list or arrays for multiple groups
            optional:
            params:
                as dictionary.
            E:
                unobserved state of the cell, list of arrays
                length of the list is the number of groups,
                each array has a shape of number of cells x number of measur
            E0, F0, numtraj, numberOfMeasurments, sigma, Tmax:
                numbers
            times:
                all times, list of arrays.
            states:
                all states, list of 2-arrays.
    """
    folder = Path("/home/andjela/work/projects/inbio/src/LightSystem/data/")
    file = list(folder.glob('*' + pattern + '*.mat'))
    if not file:
        print(f"No such file {folder/pattern}")
        return
    if len(file) > 1:
        print('needs more precission:\n')
        for fi in file:
            print(fi)
        return
    mat = scipy.io.loadmat(file[0])

# possible fields
#   ['__header__', '__version__', '__globals__', 'T_all', 'Tmax',
#    'XX_all', 'X_all', 'Y_all', 'givenParas', 'measurestep', 'n0',
#    'numberOfMeasurements', 'numtraj', 'sigma', 'uIn_comp']
#   ['__header__', '__version__', '__globals__', 'T_all', 'Tmax',
#    'XX_all', 'X_all', 'Y_all', 'givenParas', 'measurestep', 'n0',
#    'numberOfMeasurements', 'numtraj', 'sigma', 'uIn']
#   ['__header__', '__version__', '__globals__', 'Y_comp', 'uIn_comp']
#   ['__header__', '__version__', '__globals__', 'Y_all_comp', 'uIn']

    for key in ['Y_comp', 'Y_all_comp', 'Y_all']:
        if key in mat:
            mat['F'] = list(mat.pop(key)[0])
            break
    mat['numgroups'] = len(mat['F'])
    for key in ['uIn', 'uIn_comp']:
        if key in mat:
            mat['light'] = mat.pop(key)
            break
    # optional, if it's synthetic data
    if 'givenParas' in mat:
        mat['paras'] = {'a': mat['givenParas'][0, 1],
                        'b': mat['givenParas'][0, 2],
                        's': mat['givenParas'][0, 3],
                        'm': mat['givenParas'][0, 4],
                        'h': mat['givenParas'][0, 7],
                        'c': mat['givenParas'][0, 0],
                        'n': mat['givenParas'][0, 5],
                        'k': mat['givenParas'][0, 6]}
        mat['E'] = list(mat.pop('XX_all')[0])
        mat['E0'] = mat['n0'][0, 0]
        mat['F0'] = mat['n0'][1, 0]
        # these should be renamed too
        for key in ['Tmax', 'measurestep', 'numberOfMeasurements', 
                    'numtraj', 'sigma']:
            mat[key] = mat[key][0, 0]
        # redefine all simulated times and states
        mat['times'] = []
        mat['states'] = []
        for g in np.arange(mat['numgroups']):
            for tr in np.arange(int(mat['numtraj']/mat['numgroups'])):
                tall = np.array([sim for sim in mat['T_all'][0][g][0][tr]])[0]
                mat['times'].append(tall)
                xall = np.array([np.array(sim) for sim in mat['X_all'][0][g][0][tr]])
                mat['states'].append(xall.T)
            
        # remove repeated information
        del mat['n0']
        del mat['T_all']
        del mat['X_all']
        del mat['givenParas']
    return mat


def open_matlab_mcmc_file(pattern):
    """Opens matlab file that is a results of MCMC search of parameters
    Args:
        pattern: part of the filename

    Returns:
        paramsChain: in good order '$a$ $b$ $s$ $m$ $h$ $c$ $n$ $k$'
        llChain:
        initParas:
    """
    folder = Path("../LightSystem/inference")
    file = list(folder.glob('*' + pattern + '*'))
    if not file:
        print(f"No such file {folder/pattern}")
        return
    if len(file) > 1:
        print('needs more precission:\n')
        for fi in file:
            print(fi)
        return
    mat = scipy.io.loadmat(file[0])
#    initParas = {'a': mat['givenParas'][0, 1],
#                 'b': mat['givenParas'][0, 2],
#                 's': mat['givenParas'][0, 3],
#                 'm': mat['givenParas'][0, 4],
#                 'h': mat['givenParas'][0, 7],
#                 'c': mat['givenParas'][0, 0],
#                 'n': mat['givenParas'][0, 5],
#                 'k': mat['givenParas'][0, 6]}
    
    initParas = {'a': 0,
                 'b': 0,
                 's': 0,
                 'm': 0,
                 'h': 0,
                 'c': 0,
                 'n': 0,
                 'k': 0}
    paramsChain = mat['paramsChain'][[1, 2, 3, 4, 7, 0, 5, 6]]
    llChain = mat['llChain'][0]
    return paramsChain, llChain, initParas



def plot_mcmc_chain(params, lls, filename='chain', mle=None):
    parname = 'a b s m h c n k'.split()
    plt.figure(figsize=(8, 15))
    for i in range(8):
        plt.subplot(5, 2, i+1)
        plt.plot(params[i])
        if mle:
            plt.axhline(xmin = 0, y=mle[parname[i]], color='k')
        plt.ylabel(f'$\mathbf{{{parname[i]}}}$')
        plt.ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
    plt.subplot(5, 2, 9)
    plt.plot(lls)
    plt.axvline(x=np.argmax(lls), color='k')
    plt.xlabel('iteration')
    plt.ylabel('log-likelihood')
    plt.ticklabel_format(axis='y', style='sci', useOffset=max(lls))
    plt.savefig(f'figures/{filename}.eps')
    plt.tight_layout()
    plt.show()
    return    

def plot_mcmc_chain_fixedpar(params, lls, filename='chain', mle=None):
    parname = 'a b s m h c n k'.split()
    plt.figure(figsize=(16, 4))
    j = 0
    for i in [1,3,4,5]:
        plt.subplot(1, 4, j+1)
        plt.plot(params[i])
        if mle:
            plt.axhline(xmin = 0, y=mle[parname[i]], color='k')
        plt.xlabel('iteration')
        plt.ylabel(f'$\mathbf{{{parname[i]}}}$')
        plt.ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
        j= j+1
    # plt.subplot(1, 5, 5)
    # plt.plot(lls)
    # plt.axvline(x=np.argmax(lls), color='k')
    # plt.xlabel('iteration')
    # plt.ylabel('log-likelihood')
    # plt.ticklabel_format(axis='y', style='sci', useOffset=max(lls))
    plt.savefig(f'figures/{filename}.eps')
    plt.tight_layout()
    plt.show()
    return    


def plot_densities(params, mle=None, exact=None, labelname=None, color=None):
    parname = 'a b s m h c n k'.split()
    for i in range(8):
        plt.subplot(4, 2, i+1)
        if i == 7:
            sns.kdeplot(params[i], legend=True, label=labelname, shade=True, color=color)
            plt.legend(loc='upper right')
        else:
            sns.kdeplot(params[i], legend=False, label=labelname, shade=True, color=color)
        if exact:
            plt.axvline(x=exact[parname[i]], ymin=0, color='k')
        if mle is not None:
            plt.axvline(x=mle[parname[i]], ymin=0, color=color)
        plt.xlabel(f'${parname[i]}$')
        plt.ylabel('density')
        plt.ticklabel_format(axis='y', style='sci', scilimits=(-1, 1))
        plt.ticklabel_format(axis='x', style='sci', scilimits=(-2, 2))
    plt.tight_layout()
    plt.show()
    return

def plot_densities_fixedpar(params, mle=None, exact=p_def, labelname=None, color=None, factorh=True):
    plt.figure(figsize=(8, 8))
    parname = 'a b s m h c n k'.split()
    j=0
    for i in [1,3,4,5]:            
        plt.subplot(2, 2, j+1)
        if j == 3:
            ax = sns.kdeplot(params[i], legend=True, label=labelname, shade=True, color=color)
            print("\n AX3: ",ax.get_lines())
            plt.legend(loc='upper right')
            plt.ticklabel_format(axis='y', style='sci', scilimits=(-1, 1))
            plt.ticklabel_format(axis='x', style='sci', scilimits=(-2, 2))
        elif factorh and j == 2:
            ax=sns.kdeplot(params[i]*1000,  legend=True, label=labelname, shade=True, color=color)
            if mle is not None:
                plt.axvline(x=1000*mle[parname[i]], ymin=0, color=color)
            xlabels=[str(l*0.001) for l in ax.get_xticks()]
            ax.set_xticks(ax.get_xticks())
            ax.set_xticklabels(xlabels)
            ylabels=[f'{int(l/10)}e4' if l > 0 else '0' for l in ax.get_yticks()]
            ax.set_yticks(ax.get_yticks())
            ax.set_yticklabels(ylabels)
            ax.set_ylim(-3.5,72.5)
            ax.set_xlim(3.0e-2,8.6e-2)
        else:
            sns.kdeplot(params[i], legend=False, label=labelname, shade=True, color=color)
            plt.ticklabel_format(axis='y', style='sci', scilimits=(-1, 1))
            plt.ticklabel_format(axis='x', style='sci', scilimits=(-2, 2))
        if mle is not None:
            plt.axvline(x=mle[parname[i]], ymin=0, color=color)
        plt.xlabel(f'${parname[i]}$')
        plt.ylabel('density')
        j=j+1
    plt.tight_layout()
    plt.show()
    return


def plot_scatter(params, exact=p_def):
    parname = 'a b s m h c n k'.split()
    #plt.figure(figsize=(15, 15))
    for i in range(7):
        for j in range(i+1, 8):
            plt.subplot(7, 7, 7*i+j)
            plt.scatter(params[j], params[i], alpha=0.3)
            plt.scatter(exact[parname[j]], exact[parname[i]], color='k')
            plt.ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
            plt.ticklabel_format(axis='x', style='sci', scilimits=(-2, 2))
            if j != i+1:
                plt.xticks([])
                plt.yticks([])
            if j == i+1:
                plt.xlabel(f'$\mathbf{{{parname[j]}}}$')
                plt.ylabel(f'$\mathbf{{{parname[i]}}}$')
    plt.tight_layout()
    plt.show()
    return

def plot_scatter_fixedpar(params, exact=p_def):
    plt.figure(figsize=(8, 8))
    parname = 'a b s m h c n k'.split()
    indexes = [1,3,4,5]
    for indi,i in enumerate(indexes):
        for indj in range(indi+1,4):
            plt.subplot(3, 3, 3*indi+indj)
            plt.plot(params[indexes[indj]], params[i],'o')
            plt.plot(exact[parname[indexes[indj]]], exact[parname[i]], 'o', color='k')
            plt.ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
            plt.ticklabel_format(axis='x', style='sci', scilimits=(-2, 2))
            if indj != indi+1:
                plt.xticks([])
                plt.yticks([])
            if indj == indi+1:
                plt.xlabel(f'$\mathbf{{{parname[indexes[indj]]}}}$')
                plt.ylabel(f'$\mathbf{{{parname[i]}}}$')
    plt.tight_layout()
    plt.show()
    return


# %%
def matlab_data_pattern(data_type, group, numcells):
    '''
    Create a name of data file to open with respect to how they were saved
    '''
    if data_type == 'synthetic':
        if numcells == 60:
            fnamestart = f'synthetic/1021x{numcells}-'
        else:
            fnamestart = f'synthetic/1021x{numcells}-t0-'
        if group == 0:
            fnameend = 'allGroups'
        else:
            fnameend = f'Group{group+1}'
    pattern = fnamestart+fnameend
    return pattern


def mle(params, lls, burnin=0):
    '''
    take parameters that have the maximum likelihood in a given MCMC chain 
    '''
    params = params[:, burnin:]
    lls = lls[burnin:]
    ind = np.argmax(lls)
    mle = {'a': params[0, ind],
           'b': params[1, ind],
           's': params[2, ind],
           'm': params[3, ind],
           'h': params[4, ind],
           'c': params[5, ind],
           'n': params[6, ind],
           'k': params[7, ind]}
    return mle


def last(params):
    '''
    take parameters that have been the last ones usedin the MCMC chain 
    '''
    mle = {'a': params[0, -1],
           'b': params[1, -1],
           's': params[2, -1],
           'm': params[3, -1],
           'h': params[4, -1],
           'c': params[5, -1],
           'n': params[6, -1],
           'k': params[7, -1]}
    return mle



def test_hill_function():
    y, hill = hill_function(5, [0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0], 1.5, 3, 0.2)
    y_expected = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.51791, 0.63348, 0.65926, 0.66501, 0.6663, 0.66658, 0.66665, 0.66666, 0.66667, 0.66667, 0.66667, 0.66667, 0.66667, 0.66667, 0.66667, 0.14875, 0.033191, 0.007406, 0.0016525, 0.00036872, 8.2273e-05, 1.8358e-05, 4.0961e-06, 9.1397e-07, 2.0393e-07, 0.51791, 0.63348, 0.65926, 0.66501, 0.6663, 0.66658, 0.66665, 0.66666, 0.66667, 0.66667, 0.66667, 0.66667, 0.66667, 0.66667, 0.66667, 0.14875, 0.033191, 0.007406, 0.0016525, 0.00036872])
    hill_expected = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.98322, 0.99076, 0.9918, 0.992, 0.99205, 0.99206, 0.99206, 0.99206, 0.99206, 0.99206, 0.99206, 0.99206, 0.99206, 0.99206, 0.99206, 0.58135, 0.015192, 0.00017134, 1.9037e-06, 2.1149e-08, 2.3494e-10, 2.61e-12, 2.8994e-14, 3.2209e-16, 3.5782e-18, 0.98322, 0.99076, 0.9918, 0.992, 0.99205, 0.99206, 0.99206, 0.99206, 0.99206, 0.99206, 0.99206, 0.99206, 0.99206, 0.99206, 0.99206, 0.58135, 0.015192, 0.00017134, 1.9037e-06, 2.1149e-08])
    assert(len(y)==len(y_expected))
    assert np.allclose(y, y_expected, rtol = 0.0001)
    assert np.allclose(hill, hill_expected)
    
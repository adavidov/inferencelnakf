#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 25 16:02:10 2020

@author: andjela
"""
# %%
light = [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
t = np.arange(0,(1+len(light))*6,6)
plt.step(t, [0]+light)

fakea = 100
tr = simulate_ccas_ccar_system(6, light, numtraj = 1, fakea=fakea)
o = [f for _, _, _, f in tr[0]]
time = [t for t, _, _, _ in tr[0]]
plt.step(time, o, label=fakea)
plt.legend()